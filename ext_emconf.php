<?php

$EM_CONF[$_EXTKEY] = array(
    'title' => 'Cropped and better visible content elements',
    'description' => 'Adds a better preview for the content elements: html, table and bullets.',
    'category' => 'plugin',
    'author' => 'Christian Stern',
    'author_email' => 'christian.stern@pornofilm-produzent.de',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '0.3.1',
    'constraints' => array(
        'depends' => array(
            'typo3' => '7.6.0-8.7.99'
        ),
        'conflicts' => array(),
        'suggests' => array()
    ),
);
