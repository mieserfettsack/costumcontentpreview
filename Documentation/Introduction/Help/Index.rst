.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../../Includes.txt

.. _help:

=================
Do you need help?
=================

.. only:: html

There are several ways to get support for EXT:costumcontentpreview!

E-Mail
======

Just drop me a line: christian.stern@pornofilm-produzent.de

New features for sponsoring
---------------------------
You need a stunning new feature which is not yet implemented? Try to contact me! Maybe we'll find a way!
