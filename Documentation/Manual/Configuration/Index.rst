.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../../Includes.txt

.. _configuration:

=============
Configuration
=============

Change the settings of costumcontentpreview
===========================================

You can change the settings by clicking on **Admins Tools**, **Settings**, **Extension Configuration** and
**costumcontentpreview**. Inside the tab **Basic** you can find all available settings.

If you set the amount of lines to **0** the content will not be cropped, but still rendered nicely.

Number of lines after the "HTML" content element is cropped
-----------------------------------------------------------

The default value is: 9

Number of lines after the "Bullet List" content element is cropped
------------------------------------------------------------------

The default value is: 5

Number of lines after the "Table" content element is cropped
------------------------------------------------------------

The default value is: 6

Number of chars after a header is cropped
-----------------------------------------

If the field header is empty, the backend preview will use the content of bodytext to create a headline. This can lead
to very ugly previews.

The default value is: 120

Enable tt_content_drawItem hook to use templates with mod.web_layout.tt_content.preview
---------------------------------------------------------------------------------------

If you would like to use your own templates and previews please disable this option because the hook will not allow
templates defined with mod.web_layout.tt_content.preview. This extension provides templates inside the folder `Resources/Private/Templates`
