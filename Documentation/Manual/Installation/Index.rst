.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../../Includes.txt

.. _installation:

============
Installation
============

Install via composer
====================

`composer require mdy/costumcontentpreview`

Install via TYPO3 Extension Manager
===================================

You can install costumcontentpreview via Extension Manager inside your TYPO3 installation. Just go to Extension Manager
by clicking on **Extensions**. Than choose **Get Extensions** and search for **costumcontentpreview**.

Click the **Import and Install** button.

Install via Release downloads
=============================

Go to https://gitlab.com/mieserfettsack/costumcontentpreview/-/tags and pick one of the releases available. Download it
and upload it to you TYPO3 via FTP, SSH or whatever.
