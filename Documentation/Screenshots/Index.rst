.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. _screenshots:

===========
Screenshots
===========

.. only:: html

Screenshots to show you, how this extension changes the default preview in backend and how to change its behaviour.

.. figure:: ../Images/costumcontentpreview_preview_20_10_2019.png
    :align: left
    :width: 600px
    :alt: Preview of the following elements: html,table,bullets(ul,dl)

.. figure:: ../Images/costumcontentpreview_settings_20_10_2019.png
    :align: left
    :width: 600px
    :alt: Change behaviour of costumcontentpreview inside "Settings > Extension Configuration"
