<?php

use TYPO3\CMS\Core\Utility\GeneralUtility;

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$extensionConfiguration = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['costumcontentpreview']);

if ($extensionConfiguration['enableContentPreviewHook'] === "1") {
    $GLOBALS
    ['TYPO3_CONF_VARS']
    ['SC_OPTIONS']
    ['cms/layout/class.tx_cms_layout.php']
    ['tt_content_drawItem']
    ['costumcontentpreview'] = Mdy\Costumcontentpreview\Hooks\PageLayoutView::class;
}


$GLOBALS
['TBE_STYLES']
['skins']
['costumcontentpreview'] = array(
    'name' => 'costumcontentpreview',
    'stylesheetDirectories' => array(
        'css' => 'EXT:costumcontentpreview/Resources/Public/CSS/typo3_backend/'
    )
);
