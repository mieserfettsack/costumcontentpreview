<?php

namespace Mdy\Costumcontentpreview\Hooks;

use Mdy\Costumcontentpreview\Render\RenderPreviews;
use TYPO3\CMS\Backend\View\PageLayoutViewDrawItemHookInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

class PageLayoutView implements PageLayoutViewDrawItemHookInterface
{
    /**
     * Preprocesses the preview rendering of a content element.
     *
     * @param \TYPO3\CMS\Backend\View\PageLayoutView $parentObject Calling parent object
     * @param boolean $drawItem Whether to draw the item using the default functionalities
     * @param string $headerContent Header content
     * @param string $itemContent Item content
     * @param array $row Record row of tt_content
     * @return void
     */
    public function preProcess(\TYPO3\CMS\Backend\View\PageLayoutView &$parentObject, &$drawItem, &$headerContent, &$itemContent, array &$row)
    {
        /* @var RenderPreviews $RenderPreviews */
        $RenderPreviews = GeneralUtility::makeInstance('Mdy\Costumcontentpreview\Render\RenderPreviews');
        $extensionConfiguration = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['costumcontentpreview']);

        if ($row['CType'] === 'html') {
            $headerContent = '<b>' . PageLayoutView::cropHeader($row['header'], $row['bodytext']) . '</b><br>';
            $itemContent = $RenderPreviews->output(
                $row['bodytext'],
                [
                    'linesToCrop' => $extensionConfiguration['cropHTMLAfterLines'],
                    'contentWrap' => '<pre class="ccpPreWrap">|</pre>',
                    'enableHtmlSpecialChars' => true
                ]
            );
            $drawItem = FALSE;
        }

        if ($row['CType'] === 'table') {
            $headerContent = '<b>' . PageLayoutView::cropHeader($row['header'], $row['bodytext']) . '</b><br>';
            $itemContent = $RenderPreviews->output(
                $row['bodytext'],
                [
                    'linesToCrop' => $extensionConfiguration['cropTableAfterLines'],
                    'contentWrap' => '<table class="ccpTableWrap">|</table>',
                    'lineWrap' => '<tr>|</tr>',
                    'itemWrap' => '<td class="ccpTableTd">|</td>',
                    'explodeItems' => '|'
                ]
            );
            $drawItem = FALSE;
        }

        if ($row['CType'] === 'bullets') {
            $headerContent = '<b>' . PageLayoutView::cropHeader($row['header'], $row['bodytext']) . '</b><br>';
            switch ($row['bullets_type']) {
                case 0:
                    $itemContent = $RenderPreviews->output(
                        $row['bodytext'],
                        [
                            'linesToCrop' => $extensionConfiguration['cropBulletsAfterLines'],
                            'contentWrap' => '<ul class="ccpUlWrap">|</ul>',
                            'lineWrap' => '<li>|</li>'
                        ]
                    );
                    $drawItem = FALSE;
                    break;
                case 1:
                    $itemContent = $RenderPreviews->output(
                        $row['bodytext'],
                        [
                            'linesToCrop' => $extensionConfiguration['cropBulletsAfterLines'],
                            'contentWrap' => '<ol class="ccpUlWrap">|</ol>',
                            'lineWrap' => '<li>|</li>'
                        ]
                    );
                    $drawItem = FALSE;
                    break;
                case 2:
                    $itemContent = $RenderPreviews->output(
                        $row['bodytext'],
                        [
                            'linesToCrop' => $extensionConfiguration['cropBulletsAfterLines'],
                            'contentWrap' => '<dl class="ccpDlWrap">|</dl>'
                        ]
                    );
                    $drawItem = FALSE;
                    break;
            }
        }
    }

    /**
     * cropHeader the preview rendering of a content element.
     *
     * @param string $header Content of field header
     * @param string $bodytext Content of field bodytext
     * @return string
     */
    public function cropHeader($header, $bodytext)
    {
        $extensionConfiguration = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['costumcontentpreview']);

        if ($header == '' && $bodytext != '') {
            /* @var ContentObjectRenderer $cObject */
            $cObject = GeneralUtility::makeInstance('TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer');
            $max_length = $extensionConfiguration['cropHeaderAfterChars'];
            $strippedBodytext = $cObject->stdWrap_stripHtml($bodytext);

            if (strlen($strippedBodytext) > $max_length) {
                $offset = ($max_length - 3) - strlen($strippedBodytext);
                $croppedHeader = substr($strippedBodytext, 0, strrpos($strippedBodytext, ' ', $offset)) . '...';

                return $croppedHeader;
            } else {
                return $strippedBodytext;
            }

        } else {
            return $header;
        }
    }
}
