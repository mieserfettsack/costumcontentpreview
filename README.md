# costumcontentpreview

This TYPO3 extensions provides a better visible preview of the following content element: HTML, table and bullet lists

costumcontentpreview crops every content to configurable amount of lines. It also crops the headline, if the headline is
created out of bodytext because of being empty.

costumcontentpreview provides a viewhelper `croppedPreviewOfContent` which can be used  when using own templates for content elemets via 
mod.web_layout.tt_content.preview.

## Installation

This extension only works with TYPO3. Pick one of the following ways to install this extension to you TYPO3 page. Once
installed the extension runs out of the box.

#### Install via composer

`composer require mdy/costumcontentpreview`

#### Install via TYPO3 Extension Manager

You can install costumcontentpreview via Extension Manager inside your TYPO3 installation. Just go to Extension Manager
by clicking on **Extensions**. Than choose **Get Extensions** and search for **costumcontentpreview**.

Click the **Import and Install** button.

#### Install via Release downloads

Go to https://gitlab.com/mieserfettsack/costumcontentpreview/-/tags and pick one of the releases available. Download it
and upload it to you TYPO3 via FTP, SSH or whatever. 

### Develop and test extension

[ddev](https://github.com/drud/ddev) is required! You can easily develop and test the extension using ddev on your local
machine. Git clone the exentension and run the follwing command:

`ddev createDevEnvironment`

This will create a fresh installation of TYPO3 with costumcontentpreview installed and ready to test. Point your browser
to `http://costumcontentpreview.ddev.site` when the script is done.
